package model;

public class Customer {
    public String name;
    public int age, id;

    public Customer(String name, int age, int id) {
        this.name = name;
        this.age = age;
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Customer() {};

    public String toString(){
        String info = String.format("name = %s, age = %d, id = %d", name, age, id);
        return info;
    }
}