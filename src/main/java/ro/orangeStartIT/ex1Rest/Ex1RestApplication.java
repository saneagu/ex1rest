package ro.orangeStartIT.ex1Rest;

import controller.WebController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackageClasses = WebController.class)
@SpringBootApplication
public class Ex1RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ex1RestApplication.class, args);
	}
}
