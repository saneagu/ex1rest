package controller;

import model.Customer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {
    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String post(@RequestBody Customer cust) {
        System.out.println("/POST request, cust: " + cust.toString());
        return "/Post Successful!";
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Customer get(@RequestParam("name") String name, @RequestParam("age") int age, @RequestParam("id") int  id  ) {
        String info = String.format("/GET info: name=%s, age=%d, id=%d", name, age, id);
        return new Customer(name, age, id);
    }

    @RequestMapping(value= "/put/{id}", method = RequestMethod.PUT)
    public void put(@PathVariable(value = "id") int id, @RequestBody Customer cust) {
        String info = String.format("id = %d, cust = %s", id, cust.toString());
        System.out.println("/PUT info " + info);
    }

    @RequestMapping(value= "/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") int id) {
        String info = String.format("/Delete info: id = %d", id);
        System.out.println(info);
    }
}